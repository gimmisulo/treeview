//
//  main.m
//  TreeView
//
//  Created by Sulochana Premadasa on 3/28/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
